/*
	App: Booking System API

	Scenario: 
		A course booking system application where a user can enroll into a course

	Type: Course Booking System (Web App)

	Description:

		A course booking system application where a user can enroll into a course

		Allows an admin to do CRUD operations

		Allows users to register into our database

	Features:
	
		- User Registration
		- User Authentication (User Login)

		Customer / Authenticated Users:
		- View Courses (All Active Courses)
		- Enroll Course

		Admin Users:
		- Add Course
		- Update Course
		- Archive / Unarchive a Course (soft delete / reactivate the course)
		- View Courses (All courses active / inactive)
		- View / Manage User Accounts**s

		All Users (guests, customers, admin)
		- View Active Courses


*/

//Data Models for the Booking System
//Two-way Embedding

/*
	user {
		
		id - unique identifier for the document
		firstName,
		lastName,
		email,
		password,
		mobileNumber,
		isAdmin
		enrollments: [
			
			id - document identifier
			courseId - unique identifier for course
			courseName - optional
			status,
			dateEnrolled - optional
		]
	}

*/

/*
	course {
		
		id - unique identifier
		name,
		description,
		price,
		isActive,
		createdOn,
		enrollees: [
			
			id - document identifier
			userId,
			isPaid,
			dateEnrolled - optional
		]
	}

*/

