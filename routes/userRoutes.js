const express = require("express");
const router = express.Router();

const userController = require("../controllers/userControllers");

const auth = require("../auth");

//Route for checking if the user's email already exists in the db
//invoke the checkEmailExists function from the controller file later to communicate with our db
//passes the body property of our "request" object to the corresponding controller function
router.post("/checkEmail",(req,res)=>{
	userController.checkEmailExists(req.body).then(resultFromController=>res.send(resultFromController));
})

//Route for user registration

router.post("/register",(req,res)=>{

	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController))
});


//Route for user Authentication
router.post("/login",(req,res)=>{

	userController.loginUser(req.body).then(resultFromController=>res.send(resultFromController));
})

//Route for retrieving  user details
router.get("/details",auth.verify,(req,res)=>{

	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({userId:userData.id}).then(resultFromController=>res.send(resultFromController));
})


// //Route to enroll a user to a course

// router.post("/enroll",(req,res)=>{

// 	let data = {

// 		userId: req.body.userId,
// 		courseId: req.body.courseId
// 	}

// 	userController.enroll(data).then(resultFromController=>res.send(resultFromController));
// })

//Route to enroll a user to a course

router.post("/enroll",auth.verify,(req,res)=>{

	let data = auth.decode(req.headers.authorization);

	userController.enroll({userId:data.id, courseId: req.body.courseId}).then(resultFromController=>res.send(resultFromController));
})

//Allows us to export the router object that will be accessed in our "index.js"
module.exports = router;