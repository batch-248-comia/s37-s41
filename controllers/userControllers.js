//use User Model - import 
const User = require("../models/User");
const Course = require("../models/Course");

const bcrypt = require("bcrypt");
const auth = require("../auth");


//Check if the email already exists
/*
	Steps:
	1. Use mongoose "find" method to find a duplicate email
	2. Use the "then" method to send a response back to the frontendbased on the result of the "find" method
*/

module.exports.checkEmailExists = (reqBody) => {

	//the result is sent back to the front end via the then method found in the route file

	return User.find({email: reqBody.email}).then(result=>{
		
		//the find method returns a record if a match is found
		if(result.length>0){
			return true;
		//if no duplicate email found
		//user is not registered in the db
		}else{
			return false
		}
	})
};


//User Registration
/*
	Steps:
	1. Create a new user object using the mongoose model and the information from the reqBody
	2. Make sure the password is encrypted
	3. Save the new User to the database
*/

module.exports.registerUser = (reqBody) =>{

	let newUser = new User({

		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		//implement bcrypt, 10 rounds of hashing
		password: bcrypt.hashSync(reqBody.password,10)
	})

	return newUser.save().then((user,error)=>{

		if(error){
			return false
		}else{
			return true
		}
	})
}

//User Authentication
/*
	Steps:
	1. Check the databasa if the user email exists
*/

module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result=>{

		if(result == null){

			return false;

		}else{

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect){

					return {access: auth.createAccessToken(result)}
			}else{
				return	false;
			}
		}
	})
}

//Retrieve User Details

module.exports.getProfile = (data) =>{

	return User.findById(data.userId).then(result=>{
		
		result.password = "";
		return result;
	})
};


//Enroll user to a course
/*
	Steps:
	1. Find the document in the db using the user's id
	2. add the course id to the user's enrollment array
	3. Update the document in the MongoDB Atlas
*/

//async await will be used in enrolling the user because we will need to update 2 separate documents when enrolling a user
module.exports.enroll = async (data) =>{

	let isUserUpdated = await User.findById(data.userId).then(user=>{

		user.enrollments.push({courseId:data.courseId});

		return user.save().then((user,error)=>{

			if(error){
				return false;
			}else{
				return true;
			};
		});
	});

	let isCourseUpdated = await Course.findById(data.courseId).then(course=>{

		course.enrollees.push({userId: data.userId});

		return course.save().then((course,error)=>{

			if(error){
				return false;
			}else{
				return true
			};
		});
	});


	if(isUserUpdated && isCourseUpdated){
		return true;
	}else{
		return false;
	};

};

